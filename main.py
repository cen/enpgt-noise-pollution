import ast
import math
import os
import shutil
import sys
from datetime import datetime
from multiprocessing import Pool
from pathlib import Path
from typing import NamedTuple, Union

import numpy as np
import shapely.ops
from enpgt_task_file_reader import convert_nsv_task_file_to_polygons
from matplotlib import pyplot as plt
from pyproj import Transformer
from shapely import MultiPoint, MultiPolygon, Polygon
from tqdm import tqdm

from utils import get_image
from write_readme import write_readme

COLOR_CODES_LDEN = {
    0x00000000: "<55 dB",
    0xFFC74AFF: "55-60 dB",
    0xFF6600FF: "60-65 dB",
    0xFF3333FF: "65-70 dB",
    0x990033FF: "70-75 dB",
    0xAD9AD6FF: ">75 dB",
}

COLOR_CODES_LNIGHT = {
    0x00000000: "<50 dB",
    0xFFFF00FF: "50-55 dB",
    0xFFC74AFF: "55-60 dB",
    0xFF6600FF: "60-65 dB",
    0xFF3333FF: "65-70 dB",
    0x990033FF: ">70 dB",
}

COLOR_CODES_RAILWAY_LNIGHT = {
    0x00000000: "<50 dB",
    0xFFC74AFF: "50-55 dB",
    0xFF6600FF: "55-60 dB",
    0xFF3333FF: "60-65 dB",
    0x990033FF: "65-70 dB",
    0xAD9AD6FF: ">70 dB",
}

BASE_PARAMS = {
    "request": "GetMap",
    "service": "WMS",
    "crs": "EPSG:3035",
    "format": "png",
}

LAYERS = [
    "NoiseContours_air_lden:ClassifiedLden",
    "NoiseContours_ind_lden:ClassifiedLden",
    "NoiseContours_rail_lden:ClassifiedLden",
    "NoiseContours_road_lden",
    "NoiseContours_air_lnight:Classified_Lnight",
    "NoiseContours_ind_lnight:Classified_Lnight",
    "NoiseContours_rail_lnight:ClassifiedLden",
    "NoiseContours_road_lnight:Classified_Lnight",
]

WGS_UTM_TRANSPROJ = Transformer.from_crs("WGS84", "EPSG:3035", always_xy=True)

PLOT = False
PLOT_SHOW = False


class SingleResult(NamedTuple):
    geo_id: str
    area: float
    class_percentages: dict[str, float]


def do_noise_pollution(geo_id: str, layer: str, area_poly: Union[Polygon, MultiPolygon]) -> SingleResult:
    correct_color_codes = COLOR_CODES_LDEN if "_lden" in layer.lower() else COLOR_CODES_LNIGHT
    if layer == "NoiseContours_rail_lnight:ClassifiedLden":
        correct_color_codes = COLOR_CODES_RAILWAY_LNIGHT  # There is a bug where these use the LDEN color scheme

    base_url = (
        f"https://noise.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/{layer.split(':')[0]}"
        "/ImageServer/WMSServer"
    )
    img_path = Path(f"{geo_id}_{layer.replace(':','_')}_{area_poly.__hash__()!s}.png")

    min_x, min_y, max_x, max_y = area_poly.bounds

    min_x = math.floor(min_x - 1)
    min_y = math.floor(min_y - 1)
    max_x = math.ceil(max_x + 1)
    max_y = math.ceil(max_y + 1)

    width = max_x - min_x
    height = max_y - min_y
    shorter_side = min(width, height)

    width = width * (width / shorter_side)
    height = height * (height / shorter_side)

    width_in_pixels = round(width / shorter_side * 100)
    height_in_pixels = round(height / shorter_side * 100)

    params = BASE_PARAMS.copy()
    params["BBOX"] = f"{min_y},{min_x},{max_y},{max_x}"
    params["width"] = str(width_in_pixels)
    params["height"] = str(height_in_pixels)
    params["layers"] = layer

    image = get_image(base_url, params)

    assert image is not None

    image.save(img_path)
    array = np.asarray(image)
    local_values = array.view(dtype=np.uint32).reshape(array.shape[:-1]).byteswap()

    os.remove(img_path)

    # get containings
    pixel_width = shorter_side / 100
    x_values = np.array([min_x + pixel_width * (0.5 + i) for i in range(width_in_pixels)])
    y_values = np.array([min_y + pixel_width * (0.5 + i) for i in range(height_in_pixels)])

    a = np.transpose([np.tile(x_values, len(y_values)), np.repeat(y_values, len(x_values))])

    b = local_values.reshape(a.shape[0], 1)

    full = np.append(a, b, axis=1)  # TODO: ACTUALLY MAKE SURE THESE ARE THE SAME y->x ORDER!

    m = MultiPoint(full)
    m = area_poly.intersection(m)

    if PLOT or PLOT_SHOW:
        points = m.geoms
        xs = [point.x for point in points]
        ys = [point.y for point in points]
        colors = [int(point.z) for point in points]
        colors = [
            (
                ((c >> 24) & 0xFF) / 256,
                ((c >> 16) & 0xFF) / 256,
                ((c >> 8) & 0xFF) / 256,
            )
            for c in colors
        ]
        plt.scatter(xs, ys, c=colors)
        if PLOT_SHOW:
            plt.show()

    values = np.fromiter((p.z for p in m.geoms), b.dtype)
    elements, counts = np.unique(values, return_counts=True)
    total = np.sum(counts)

    counts = {k.item(): v.item() for k, v in zip(elements, counts)}

    non_existing_counts = [color for color in counts if color not in correct_color_codes]
    assert not non_existing_counts, non_existing_counts

    percentages = {code: 0.0 for code in correct_color_codes.values()}
    for color, count in counts.items():
        percentages[correct_color_codes[color]] += counts.get(color, 0) / total * 100

    return SingleResult(geo_id, area_poly.area, percentages)


def extract_single_noise_pollution_star(star: tuple[str, Union[Polygon, MultiPolygon], str]) -> SingleResult:
    return do_noise_pollution(*star)


def extract_noise_pollution_for_one_layer(
    geo_id_to_polygon: dict[str, Union[MultiPolygon, Polygon]], output_file: Path, layer: str, desc: str
) -> list[SingleResult]:
    correct_color_codes = COLOR_CODES_LDEN if "lden" in layer.lower() else COLOR_CODES_LNIGHT

    starmap = [
        (geo_id, layer, shapely.ops.transform(WGS_UTM_TRANSPROJ.transform, polygon))
        for geo_id, polygon in geo_id_to_polygon.items()
    ]

    with Pool(1) as pool:
        output: list[SingleResult] = list(
            tqdm(
                pool.imap(extract_single_noise_pollution_star, starmap),
                total=len(geo_id_to_polygon),
                desc=f"Extracting Noise Pollution Contours for layer {layer} and {desc}",
            )
        )

    with open(output_file, "w") as output_file:
        output_file.write(",".join(["geo_id", "area", *dict.fromkeys(correct_color_codes.values()).keys()]))
        output_file.write("\n")

        for single_result in output:
            output_file.write(
                ",".join(
                    str(tok)
                    for tok in (
                        single_result.geo_id,
                        single_result.area,
                        *single_result.class_percentages.values(),
                    )
                )
            )
            output_file.write("\n")

    return output


def extract_noise_pollution(task_file: Path, output_dir: Path, radius: int):
    output_dir.mkdir(parents=True, exist_ok=True)
    shutil.copyfile(task_file, output_dir / task_file.name)
    geo_id_to_polygon = convert_nsv_task_file_to_polygons(task_file, radius)

    for layer in LAYERS:
        output_file = output_dir / layer.split(":")[0] / f"output_{radius}.csv"
        output_file.parent.mkdir(parents=True, exist_ok=True)
        extract_noise_pollution_for_one_layer(geo_id_to_polygon, output_file, layer, desc=f"radius {radius}")


def main(args: list[str]) -> int:
    default_task_file = Path("task.txt")

    radii = [100, 200, 500, 1000, 2000]

    for arg in args:
        if arg.startswith("[") and arg.endswith("]"):
            radii = ast.literal_eval(arg)
            continue

        if arg.isnumeric():
            radii = [int(arg)]
            continue

        arg_as_path = Path(arg)
        if arg_as_path.is_file():
            default_task_file = arg_as_path
            continue

        return -1

    task_file = Path(default_task_file)

    if not task_file.is_file():
        return -1

    output_dir = Path("outputs", datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))

    for radius in radii:
        extract_noise_pollution(task_file, output_dir, radius)

    with open(output_dir / "readme.txt", "w") as f:
        write_readme(f, radii)

    return 0


if __name__ == "__main__":
    main(sys.argv[1:])
