import logging
import os
import random
from pathlib import Path
from time import sleep

import numpy as np
import rasterio
from PIL.Image import Image
from rasterio import transform

from utils import get_image_from_params

COLOR_CODES_LDEN = {
    0x00000000: 0,
    0xFFC74AFF: 1,
    0xFF6600FF: 2,
    0xFF3333FF: 3,
    0x990033FF: 4,
    0xAD9AD6FF: 5,
}


MIN_X = 4000000
MAX_X = 4700000
MIN_Y = 2600000
MAX_Y = 3600000
WIDTH = 100
PIXEL_SIZE = 10


def write_geotiff(
    tif_path: Path, min_x: int, max_y: int, pixel_width: int, pixel_height: int, class_array: np.ndarray
) -> None:
    # Write it out to a file.

    dst_transform = transform.from_origin(min_x, max_y, pixel_width, pixel_height)

    try:
        with rasterio.Env():
            with rasterio.open(
                tif_path,
                "w",
                driver="GTiff",
                width=class_array.shape[1],
                height=class_array.shape[0],
                count=1,
                dtype=np.uint8,
                nodata=255,
                transform=dst_transform,
                crs="EPSG:3035",
            ) as dst:
                dst.write(class_array, indexes=1)
    except Exception as e:
        if tif_path.is_file():
            os.remove(tif_path)
        raise e


def convert_to_class_array(tif_path: Path, image: Image) -> np.ndarray:
    array = np.asarray(image)
    array = array.view(dtype=np.uint32).reshape(array.shape[:-1]).byteswap()

    replaced = np.zeros_like(array, dtype=np.uint8)
    sanitycheck = np.zeros_like(array, dtype=bool)
    for key, value in COLOR_CODES_LDEN.items():  # TODO: CHOOSE BY LAYER!
        replaced[array == key] = value
        sanitycheck[array == key] = True
    assert np.all(sanitycheck)
    return replaced


def scrape_geotiff(output_dir: Path, min_x: int, min_y: int, layer_name: str):
    max_x = min_x + WIDTH * PIXEL_SIZE
    max_y = min_y + WIDTH * PIXEL_SIZE

    tif_path = output_dir / f"{min_x}-{max_x}_{min_y}-{max_y}.tif".replace(":", "_")

    if tif_path.exists():
        logging.warning(f"Geotiff already downloaded. x={min_x} y={min_y}, layer={layer_name}")
        return

    logging.info(f"Downloading geotiff. x={min_x} y={min_y}, layer={layer_name}")

    image = get_image_from_params(min_x, max_x, min_y, max_y, WIDTH, WIDTH, layer_name)
    class_array = convert_to_class_array(tif_path, image)
    write_geotiff(tif_path, min_x, max_y, PIXEL_SIZE, PIXEL_SIZE, class_array)


def scrape_geotiffs(layer_name: str):
    output_dir = Path("scraped_geotiffs", layer_name)
    output_dir.mkdir(parents=True, exist_ok=True)

    all_pictures = []
    for x in range(MIN_X, MAX_X, WIDTH * PIXEL_SIZE):
        for y in range(MIN_Y, MAX_Y, WIDTH * PIXEL_SIZE):
            all_pictures.append((x, y))
    random.shuffle(all_pictures)

    for x, y in all_pictures:
        scrape_geotiff(output_dir, x, y, layer_name)
        sleep(random.randrange(2, 7))


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.INFO)
    scrape_geotiffs("NoiseContours_road_lden")
