import os.path
import sys
from dataclasses import dataclass, field
from pathlib import Path

from BaseClasses import ToolDefinition, ToolDefinitionArgs

sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from tools.NoisePollutionTool.main import main


class NoisePollutionToolClass(ToolDefinition):
    tool_name: str = "NoisePollutionTool"
    metric_name: str = "Noise Pollution"
    supports_multipoly_mode = False

    @dataclass
    class NoisePollutionToolArgs(ToolDefinitionArgs):
        radii: list[int] = field(default_factory=lambda: [100, 200, 500, 1000, 2000])

    @classmethod
    def supports_coordinate(cls, lat: float, lng: float) -> bool:
        return -23.499695 < lng < 70.773933 and 23.900132 < lat < 65.977330

    @classmethod
    def required_setup(cls, _: NoisePollutionToolArgs):
        return

    @classmethod
    def main(cls, args: NoisePollutionToolArgs) -> Path:
        sysargs = [str(args.radii).replace(" ", ""), os.path.realpath(args.task_file_location)]

        correct_dir = os.getcwd()

        tool_dir = os.path.dirname(os.path.realpath(__file__))

        output_directories_before = []
        if os.path.isdir(os.path.join(tool_dir, "outputs")):
            output_directories_before = [
                Path(tool_dir, "outputs", p) for p in os.listdir(os.path.join(tool_dir, "outputs"))
            ]

        os.chdir(tool_dir)

        main(sysargs)

        os.chdir(correct_dir)

        output_directories_after = []
        if os.path.isdir(os.path.join(tool_dir, "outputs")):
            output_directories_after = [
                Path(tool_dir, "outputs", p) for p in os.listdir(os.path.join(tool_dir, "outputs"))
            ]

        new_output_dirs = list(set(output_directories_after) - set(output_directories_before))
        assert len(new_output_dirs) == 1

        # TODO: Write readme

        return new_output_dirs[0]
