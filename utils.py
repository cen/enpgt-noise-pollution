from functools import lru_cache
from io import BytesIO
from logging import error, warning
from typing import Optional
from urllib.parse import urlencode

from PIL import Image
from requests import Session
from requests import codes as request_codes
from requests.adapters import HTTPAdapter
from urllib3 import Retry

BASE_PARAMS = {
    "request": "GetMap",
    "service": "WMS",
    "crs": "EPSG:3035",
    "format": "png",
}


@lru_cache
def get_adapter():
    retry = Retry(total=200, backoff_factor=0.1)
    adapter = HTTPAdapter(max_retries=retry)

    session = Session()
    session.mount("https://", adapter)

    return adapter, session


def get_image(url, params) -> Optional[Image]:
    adapter, session = get_adapter()
    url = url + "?" + urlencode(params)

    for i in range(1, 31):
        try:
            response = session.get(url)
            if response.status_code == request_codes.ok:
                return Image.open(BytesIO(response.content))

            warning(f"Noise image request unexpectedly failed. This happens sometimes. Retrying... {i}/30")
        except ValueError:
            return None

    session.close()
    error("Noise image request unexpectedly failed, max retries exceeded!")
    return None


def get_image_from_params(
    min_x: int, max_x: int, min_y: int, max_y: int, width: int, height: int, layer_name: str
) -> Optional[Image]:
    base_url = (
        f"https://noise.discomap.eea.europa.eu/arcgis/services/noiseStoryMap/{layer_name.split(':')[0]}"
        "/ImageServer/WMSServer"
    )
    params = BASE_PARAMS.copy()
    params["BBOX"] = f"{min_y},{min_x},{max_y},{max_x}"
    params["width"] = str(width)
    params["height"] = str(height)
    params["layers"] = layer_name

    return get_image(base_url, params)
