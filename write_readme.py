from typing import TextIO


def write_readme(f: TextIO, radii: list[int]):
    f.write(
        "Noise Pollution Tool\n\n"
        "This file contains some information on how the data was generated and how to interpret it."
    )

    f.write(
        "\n\n-----\n\n"
        "EEA Noise Pollution Dataset\n\n"
        "This tool uses an Noise Pollution Dataset from the German Environmental Agency from 2017.\n"
        "Central to this dataset is an image server for Noise Bands using vector graphics.\n"
        "The value of each pixel denotes the amount of one type of noise experienced in that location.\n"
        "You can view this dataset at:\n"
        "https://www.eea.europa.eu/en/datahub/datahubitem-view/c952f520-8d71-42c9-b74c-b7eb002f939b"
    )

    f.write(
        "\n\n-----\n\n"
        "EEA NOISE POLLUTION METRICS\n\n"
        "This output contains several folders, each for one specific metric.\n\n"
        "Importantly, noise is only given in discrete bands like 60-65dB.\n"
        "This means we are dealing with an ordinal scale and a mean cannot be calculated."
        " Instead the results are given in the ordinal classes.\n\n"
        "This is condensed information without warranty. You should cross reference the original information:\n"
        "https://www.eea.europa.eu/en/datahub/datahubitem-view/c952f520-8d71-42c9-b74c-b7eb002f939b\n\n"
        "NoiseContours_air_lden:ClassifiedLden - Air traffic noise during the daytime\n"
        "NoiseContours_ind_lden:ClassifiedLden - Industry noise during the daytime\n"
        "NoiseContours_rail_lden:ClassifiedLden - Railway noise during the daytime\n"
        "NoiseContours_road_lden - Road&Traffic noise during the daytime\n"
        "NoiseContours_air_lnight:Classified_Lnight - Air traffic noise during nighttime\n"
        "NoiseContours_ind_lnight:Classified_Lnight - Industry noise during nighttime\n"
        "NoiseContours_rail_lnight:ClassifiedLden - Railways noise during nighttime (Inconsistency in the dataset*)\n"
        "NoiseContours_road_lnight:Classified_Lnight - Road&Traffic noise during the daytime\n\n"
        "*There is an inconsistency in the dataset with nighttime railway noise. For some reason,"
        " it was colored using the daytime color scheme.\n"
        "The assumption I made when writing this tool is that it's supposed to use the nighttime color scheme"
        " and just uses the wrong colors by accident.\n"
        "If my assumption is wrong, nighttime railway noise classification may be shifted by 5dB.\n"
        "This should hopefully not have a huge impact.\n"
    )

    f.write(
        "\n-----\n\n"
        "INPUT AND OUTPUT\n\n"
        "Each line in the task file defines a location.\n"
        "The Noise Pollution Tool will extract the Noise Pollution values in a radius around each location.\n\n"
    )

    radius_text = f'In this case, the radius {radii[0]}m was used. The output can be viewed in "output_{radii[0]}.csv".'
    if len(radii) > 1:
        radius_text = (
            f"In this case, the radii {', '.join(str(radius) + 'm' for radius in radii[:-1])}"
            f" and {radii[-1]}m were used. For each radius, you'll be able to find an output file, for"
            f' example: "output_{radii[0]}.csv".'
        )

    f.write(radius_text + "\n")

    f.write(
        "An output file usually contains the id, lat and lng of each location, followed by the Noise Pollution"
        " values calculated by the tool."
    )

    f.write("\n\n-----\n\nWHAT EXACTLY WAS DONE FOR THIS TASK?\n\n")

    f.write(
        "For each search area (location+radius), an image is retrieved from each of the noise pollution servers.\n"
        "The pixels of this image are then determined to be inside or outside of the search area.\n"
        "Then, the inside pixels are classified by color, and those colors are then converted back"
        " into the noise bands they represent.\n\n"
    )

    f.write("\n-----\n\nOUTPUT\n\n")

    f.write(
        "Each output has the following parameters:\n\n"
        "geo_id - Unique identifier for location.\n"
        "area - Total area of pixel squares that fell within the radius.\n"
        'classes such as "55dB - 60dB" - Percentage of pixels falling within a particular noise band.\n'
    )
